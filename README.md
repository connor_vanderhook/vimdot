## Installation

    git clone git@bitbucket.org:jacor/vimdot.git ~/.vim
    cd ~/.vim
    git submodule update --init
    mkdir -p ~/.vim/autoload && \
    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
    ln -s ~/.vim/vimrc ~/.vimrc

## Install vim

Lua is required by the neocomplete plugin:

    brew install vim --with-lua
    
## Install ctags
    brew install ctags

## Install platinum searcher
    brew install the_platinum_searcher
    pt -i "search for something" go/src/
    
## Install macvim

Lua is required by the neocomplete plugin:

    brew install macvim --with-cscope --with-lua

## Pathogen

The setup relies on [pathogen](https://github.com/tpope/vim-pathogen) to
cleanly install separate plugins into the bundles directory.

## Bundles

Bundles are added as git submodules in the .vim/bundle directory.

- [dash.vim](https://github.com/rizzatti/dash.vim)
- [incsearch.vim](https://github.com/haya14busa/incsearch.vim)
- [neocomplete.vim](https://github.com/Shougo/neocomplete.vim)
- [nerdcommmenter](https://github.com/scrooloose/nerdcommenter)
- [nerdtree](https://github.com/scrooloose/nerdtree)
- [syntastic](https://github.com/scrooloose/syntastic)
- [tagbar](https://github.com/majutsushi/tagbar)
- [ultisnips](https://github.com/SirVer/ultisnips)
- [unite.vim](https://github.com/Shougo/unite.vim)
- [vim-airline](https://github.com/bling/vim-airline)
- [vim-colors-solarized](https://github.com/altercation/vim-colors-solarized)
- [vim-dispatch](https://github.com/tpope/vim-dispatch)
- [vim-easymotion](https://github.com/Lokaltog/vim-easymotion)
- [vim-fugitive](https://github.com/tpope/vim-fugitive)
- [vim-gitgutter](https://github.com/airblade/vim-gitgutter)
- [vim-go](https://github.com/fatih/vim-go)
- [vim-markdown](https://github.com/tpope/vim-markdown)
- [vimproc.vim](https://github.com/Shougo/vimproc.vim)

To add a new plugin, follow this example while at the repository toplevel:

    git submodule add https://github.com/scrooloose/nerdtree bundle/nerdtree

    - or -

    cd bundle
    git submodule add https://github.com/scrooloose/nerdtree

To update the submodules, run the follow the top-level of the working tree (`.vim`):

    git submodule foreach git pull origin master
    git add *
    git commit -m "Update bundles to latest releases."

or, simply:

    git submodule foreach git pull origin master && git add * && git commit -m "Update bundles to latest releases."

*Remember to execute `make` in the bundle/vimproc.vim directory.*

To remove a plugin (See <http://davidwalsh.name/git-remove-submodule>):

    python remove_plugin.py rvm

For reference:

1. Delete the relevant line from the .gitmodules file:
  `$ git config -f .gitmodules --remove-section submodule.bundle/rvm`
2. Delete the relevant section from .git/config:
  `$ git config -f .git/config --remove-section submodule.bundle/rvm`
3. Stage the changes to the .gitmodules file:
  `$ git add .gitmodules`
4. Run `git rm --cached bundle/rvm` (no trailing slash).
5. Run `rm -rf .git/modules/bundle/rvm`.
6. Commit the changes to the superproject.
7. Delete the now untracked submodule files:
  `rm -rf path/to/submodule`

### Plugins to consider

## vim-go https://github.com/fatih/vim-go


