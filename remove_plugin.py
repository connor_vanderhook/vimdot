#!/usr/bin/env python

import os
import sys
from shutil import rmtree
from subprocess import check_output

def check_plugin(name):
    return os.path.isdir("bundle/" + name)

def remove_plugin(name):
    # 1. Delete the relevant line from the .gitmodules file:
    # `$ git config -f .gitmodules --remove-section submodule.bundle/rvm`
    check_output("git config -f .gitmodules --remove-section submodule.bundle/" + name, shell = True)
    # 2. Delete the relevant section from .git/config:
    # `$ git config -f .git/config --remove-section submodule.bundle/rvm`
    check_output("git config -f .git/config --remove-section submodule.bundle/" + name, shell = True)
    # 3. Stage the changes to the .gitmodules file:
    # `$ git add .gitmodules`
    check_output("git add .gitmodules", shell = True)
    # 4. Run `git rm --cached bundle/rvm` (no trailing slash).
    check_output("git rm --cached bundle/" + name, shell = True)
    # 5. Run `rm -rf .git/modules/bundle/rvm`.
    rmtree(".git/modules/bundle/" + name)
    # 6. Commit the changes to the superproject.
    check_output("git commit -m 'Remove plugin " + name + "'", shell = True)
    # 7. Delete the now untracked submodule files:
    #     `rm -rf path/to/submodule
    rmtree("bundle/" + name)

if __name__ == '__main__':
    plugin = sys.argv[1]
    if check_plugin(plugin):
        print("removing plugin " + plugin + "...")
        remove_plugin(plugin)
    else:
        print(plugin + " is not a plugin in bundle/")
